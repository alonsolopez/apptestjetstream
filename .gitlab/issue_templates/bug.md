# Bug

## Summary

> {Escribe el resumen de la falla}

### Pasos para reproducir el BUG

> {Indica los pasos para reproducir el bug}

1.  Paso 1
1.  Paso 2
1.  Paso 3
1.  Paso 4

#### **¿Cual es el comportamiento actual?**

{Describe que es lo que sucede mal}

#### **¿Cual es el comportamiento esperado?**

{Describe que es lo que DEBERÍA de suceder correctamente}
