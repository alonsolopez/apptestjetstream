<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Alert extends Component
{
    //props
    public $color;
    public $titulo;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($color = "orange")
    {
        //pars de valor en componente
        $this->color = $color;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.alert');
    }
}