<?php

namespace App\Http\Livewire;

use App\Models\Page;
use Livewire\Component;
use Illuminate\Validation\Rule;

class Pages extends Component
{
    public $title;
    public $slug;
    public $content;
    public $modalFormVisible = false;

    /**
     * Permite obtener los datos del "modelo" en trabajo con la vista livewire
     * en este contexto de objeto, como array modelData
     *
     * @return void
     */
    public function modelData()
    {
        return [
            "title"  => $this->title,
            "slug"   => $this->slug,
            "content" => $this->content,
        ];
    }

    //validaciones
    public function rules()
    {
        return [
            "title" => ['required'],
            "slug" => ['required', Rule::unique('pages', 'slug')],
            "content" => ['required'],
        ];
    }
    protected $messages = [
        "title.required" => "Agregar título, ¿como vas a crear una page sin TíTULO?",
        "slug.required" => "Este se pone automático con el título y ya...",
        "slug.unique" => "Debe ser único, sino se confunden las páginas",
        "content.required" => "Despláyate poniendo el contenido.. usa el editor.",
    ];

    /**
     * create crea el registro con el modelo
     * de datos en el form.
     * @return void
     */
    public function create()
    {
        //se lleva a cabo la validacion con las rules definidas
        $this->validate();
        //si pasan las vals sigue adelante, si no, se devuelve con @error
        Page::create($this->modelData());
        //limpiar
        $this->resetVars();
    }
    /**
     * createShowModal, muestra el modal de Crear nueva página,
     * ponemos en true la bandera `modalFormVisible`
     *
     * @return void
     */
    public function createShowModal()
    {
        $this->modalFormVisible = true;
    }

    /**
     * Actualiza el campo, pero sobre todo, toma el valor de lo que se escriba
     * en Title en la forma, para convertirlo a SLUG
     *
     * @return void
     */
    public function updatedTitle()
    {
        $this->generateSlug($this->title);
    }

    /**
     * Genera el Slug, con el dato de lo que esté escrito en TITLE
     * Se tarda un poco, y lo muestra como quedará despues de http://localhost:8000/slug
     *
     * @param  mixed $value El dato recibido para convertirlo en Slug (reemplazando chars por '-' y todo minúscula)
     * @return void
     */
    public function generateSlug($value)
    {
        $this->slug = strtolower(str_replace([' ', ',', ';', '...', ':', '--'], '-', $value));
    }



    /**
     * El método para mostrar la vista livewire... render
     *
     * @return void
     */
    public function render()
    {
        return view('livewire.pages');
    }

    /**
     * Resetea todos los valores de este componente livewire a nul, o false, etc.
     *
     * @return void
     */
    public function resetVars()
    {
        $this->title = false;
        $this->slug = false;
        $this->content = false;
        $this->modalFormVisible = false;
    }
}//fin de clase componente livewire Page