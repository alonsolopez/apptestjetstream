<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use Livewire\Component;

class Comments extends Component
{
    public $newComment;

    public $comments = [
        [
            'body' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Facere facilis voluptates quia voluptatum dolores quae, amet voluptas incidunt atque nihil soluta corporis debitis, eius veritatis autem nam pariatur quidem vel.',
            'created_at' => 'hace 3 minutos',
            'creator' => 'Alonso lopez',
        ],
    ];
    public function render()
    {
        return view('livewire.comments');
    }

    public function addComment()
    {
        array_unshift($this->comments, [
            'body' => $this->newComment,
            'created_at' => Carbon::now()->diffForHumans(),
            'creator' => "YOOOO!",
        ]);
        $this->newComment = '';
    }
}