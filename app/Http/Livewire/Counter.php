<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Counter extends Component
{
    //var de contador
    public $count = 0;
    //arfticulos
    public $articulos = [
        'cajetilla marlboros rojos 20',
        'cajetilla marlboros blancos 20',
        'cajetilla marlboros mentolados 20',
        'cajetilla marlboros rojos 100 20',
        'cajetilla marlboros blancos 100 20',
        'cajetilla marlboros mentolados 100 20',
    ];

    public $total = 0;

    public function render()
    {
        return view('livewire.counter', ['count' => $this->count]);
    }

    public function increment()
    {
        $this->count++;
    }
    public function decrement()
    {
        $this->count - 1 < 0 ? 0 : $this->count--;
    }

    public function elegirArticulo()
    {
        $this->total += 90;
    }
}