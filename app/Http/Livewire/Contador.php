<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Contador extends Component
{

    public $contador = 0;
    public $nombre = "Alonso Lopez";

    public function incrementar()
    {
        $this->contador++;
    }
    public function decrementar()
    {
        $this->contador--;
    }

    public function render()
    {
        return view('livewire.contador');
    }
}