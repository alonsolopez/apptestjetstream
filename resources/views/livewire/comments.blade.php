<div>
    <div class="my-4 flex">
        <input type="text" wire:model="newComment" class="w-full rounded border shadow p-2 mr-2 my-2" placeholder="Escribe wey...." name="" id="">
        <div class="py-2">
            <button class="p-2 bg-blue-500 rounded shadow text-white" wire:click="addComment">Agregar</button>
        </div>
    </div>
<br>
    @foreach ($comments as $comment)
        <div class="rounded border shadow p-3 my-2">
            <p class="flex justify-start my-2">{{$comment['creator']}}</p>
            <p class="mx-3 py-1 text-xs text-gray-500 font-semibold">{{$comment['created_at']}}</p>
        </div>
        <p class="text-gray-800">{{$comment['body']}}</p>
    @endforeach
    <br>
</div>
