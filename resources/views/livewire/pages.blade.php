<div class="p-6">
    <div class="flex items-center justify-end px-4 py-3 text-right text-cool-gray-500 sm:px-6">
        <x-jet-button wire:click="createShowModal">
            {{ __('Crear') }}
        </x-jet-button>
    </div>

    {{-- Modal form --}}
        <x-jet-dialog-modal wire:model="modalFormVisible">
            <x-slot name="title">
                {{ __('Guardar Página') }}
            </x-slot>

            <x-slot name="content">
                <div class="mt-4">
                    <x-jet-label for="title" value="{{ __('Título') }}" />
                    <x-jet-input id="title" wire:change="updatedTitle" class="block mt-1 w-full" type="text" wire:model.debounce.800ms="title"   />
                    @error('title') <span class="error">{{$message}}</span> @enderror
                </div>
                <div class="mt-4">
                    <x-jet-label for="slug" value="{{ __('Slug') }}" />
                    <div class="mt-1 flex rounded-md shadow-sm">
                        <span class="inline-flex items-center px-3 rounded-1-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                            http://localhost:8000/
                        </span>
                        <x-jet-input id="slug" class="form-input flex-1 block  w-full rounded-none rounded-r-md transition duration-150 ease-in-out sm:text-sm sm:leading-5 " type="text" wire:model="slug" placeholder="El url SLUG"/>
                    </div>
                    @error('slug') <span class="error">{{$message}}</span> @enderror
                </div>
                <div class="mt-4">
                    <x-jet-label for="title" value="{{ __('Contenido') }}" />
                    <div class="mt-1 bg-white">
                        <div class="body-content px-2" wire:ignore>
                            <trix-editor class="trix-content" x-ref="trix" wire:model.debounce.100000ms="content" wire:key="trix-content-unique-key">

                            </trix-editor>
                        </div>
                    </div>
                    @error('content') <span class="error">{{$message}}</span> @enderror
                </div>

                {{-- un control de password --}}
                {{-- <div class="mt-4" x-data="{}" x-on:confirming-delete-user.window="setTimeout(() => $refs.password.focus(), 250)">
                    <x-jet-input type="password" class="mt-1 block w-3/4" placeholder="{{ __('Password') }}"
                                x-ref="password"
                                wire:model.defer="password"
                                wire:keydown.enter="deleteUser" />

                    <x-jet-input-error for="password" class="mt-2" />
                </div> --}}
            </x-slot>

            <x-slot name="footer">
                <x-jet-secondary-button wire:click="$toggle('modalFormVisible')" wire:loading.attr="disabled">
                    {{ __('Olvídalo...') }}
                </x-jet-secondary-button>

                <x-jet-button class="ml-2 bg-green-500" wire:click="create" wire:loading.attr="disabled">
                    {{ __('Guardar Página') }}
                </x-jet-button>
            </x-slot>
        </x-jet-dialog-modal>
</div>
