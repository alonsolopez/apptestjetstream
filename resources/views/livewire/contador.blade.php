<div class="container">
    {{-- A good traveler has no fixed plans and is not intent upon arriving. --}}
    <div class="container w-full flex text-center  px-8">
        <h3 class="px-5 py-6 border border-r " wire:click="incrementar">+</h3>
        <h2 class="px-5 py-6 border border-r ">{{$contador}}</h2>
        <h3 class="px-5 py-6 border border-r " wire:click="decrementar">-</h3>
    </div>
    <hr>
    <div>
        <input type="text" name="txtNombre" id="txtNombre" wire:model="nombre" placeholder="Escribe tu nombre">
    </div>
    <hr>
    <div>
        <p>NOMBRE:::{{$nombre}}</p>
    </div>
    <hr>
</div>
